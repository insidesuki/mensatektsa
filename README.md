**MensatekTsaAdapter**
==============================
---
- Adapter for Mensatek TSA over openssl
- Adapter for Mensatek TSA over lofirmo_com.jar (jar pdf stamp utility)
 

**1-Requirements**
--


- A Mensatek account with credits (https://www.mensatek.com/)
- Curl extension
- Java 1.8 (JRE to use lofirmo_com.jar utility)
- Openssl
- lofirmo_com.jar
- fnmt.png (included in lofirma_com.jar package)

1.1-Installing Java 1.8 (*nix)
---

- Download JRE binaries from https://www.oracle.com/java/technologies/downloads/#java8 (An Oracle account is needed)
- Create a directory to install JRE in with:

```
sudo mkdir /usr/local/java
```
- Move the JRE binaries into the directory:
```
sudo mv jre-8u371-linux-x64.tar.gz /usr/local/java
```
- Go into the installation directory:
```
cd /usr/local/java
```
- Unpack
```
sudo tar -zxvf jre-8u371-linux-x64.tar.gz
```
- Save disk space (optional)
```
sudo rm -f jre-8u371-linux-x64.tar.gz
```
- Let the system know where JRE is installed:
```
sudo update-alternatives --install "/usr/bin/java" "java" "/usr/local/java/jre1.8.0_371/bin/java" 1
```
- Check installation:
```
java -version
```
It should output the following:
```
java version "1.8.0_371"
Java(TM) SE Runtime Environment (build 1.8.0_371-b11)
Java HotSpot(TM) 64-Bit Server VM (build 25.371-b11, mixed mode)

```


**2-Mensatek Credential**
--
- Create an .env.local file to store the mensatek credentials:

```
MTK_URL=https://www.lofirmo.com/tsa
MTK_USER=your_user
MTK_PASSWORD=your_password
MTK_JAR_PATH=mensatek_jar_file_path

```

**3-Running tests**
--
```
 php vendor/phpunit/phpunit/phpunit
```

**4-Integration with Stamp**
--
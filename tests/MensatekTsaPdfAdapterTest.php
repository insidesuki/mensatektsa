<?php


use Insidesuki\Stamp\Adapters\AdapterConfig;
use Insidesuki\Stamp\StampFile;
use Insidesuki\Stamp\StampFolder;
use Insidesuki\Stamp\Tsa\Mensatek\Exceptions\MensatekCredentialException;
use Insidesuki\Stamp\Tsa\Mensatek\Exceptions\UnsupportFileException;
use Insidesuki\Stamp\Tsa\Mensatek\MensatekCredential;
use Insidesuki\Stamp\Tsa\Mensatek\MensatekPdfConfig;
use Insidesuki\Stamp\Tsa\Mensatek\MensatekTsaPdfAdapter;
use PHPUnit\Framework\TestCase;

class MensatekTsaPdfAdapterTest extends TestCase
{


    protected mixed $mtkUrl;
    protected mixed $mtkPassword;
    protected mixed $mtkUser;
    protected mixed $mtkJarPath;

    public function setUp(): void
    {
        $this->pathLib = __DIR__ . '/../src/Lib';
        $this->pathExamples = __DIR__ . '/../src/Docs/examples';
        $this->mtkUrl = $_ENV['MTK_URL'];
        $this->mtkUser = $_ENV['MTK_USER'];
        $this->mtkPassword = $_ENV['MTK_PASSWORD'];
        $this->mtkJarPath = $_ENV['MTK_JAR_PATH'];
        $this->fileToStamp = StampFile::fromPath($this->pathExamples . '/input.txt');
        $this->pdfFileToStamp = StampFile::fromPath($this->pathExamples . '/example.pdf');


    }

    public function testFailMensatekCustomConfigDoesNotExists(): void
    {

        $this->expectExceptionMessage('CustomConfig mensatek, does not exists!!!');
        $credential = new MensatekCredential('url', 'user', 'pass');

        $config = new AdapterConfig(StampFolder::fromPath('/var'));
        $mensatekAdapter = new MensatekTsaPdfAdapter($credential);
        $mensatekAdapter->stamp($this->fileToStamp, $config);


    }

    public function testFailWhenFileIsNotPdf(): void
    {

        $this->expectException(UnsupportFileException::class);

        $credential = new MensatekCredential('url', 'user', 'pass');
        $mensatekAdapter = new MensatekTsaPdfAdapter($credential);
        $config = new AdapterConfig(StampFolder::fromPath('/var'));
        $config->addExtraConfig(MensatekPdfConfig::CONFIG_NAME, $this->getMensatePdfConfiguration());
        $mensatekAdapter->stamp($this->fileToStamp, $config);

    }

    public function testFailWhenInvalidCrendentialsOrNotCredit(): void
    {

        $this->expectException(MensatekCredentialException::class);
        $credential = new MensatekCredential('url', 'userfail', 'passd123s');
        $mensatekAdapter = new MensatekTsaPdfAdapter($credential);
        $mensatekAdapter->stamp($this->pdfFileToStamp, $this->getValidAdapterConfig('test'));

    }

    public function testOkPdfWasSigned(): void
    {

        $credential = new MensatekCredential($this->mtkUrl, $this->mtkUser, $this->mtkPassword);
        $mensatekAdapter = new MensatekTsaPdfAdapter($credential);
        $fileStamped = $mensatekAdapter->stamp($this->pdfFileToStamp,
            $this->getValidAdapterConfig('signed'));
        $this->assertTrue($fileStamped->validStamp);

    }

    public function testOkVerify(): void
    {

        $credential = new MensatekCredential($this->mtkUrl, $this->mtkUser, $this->mtkUser);
        $mensatekAdapter = new MensatekTsaPdfAdapter($credential);
        $pdfToVerify = StampFile::fromPath($this->pathExamples. '/example_signed.pdf');
        $stampFile = $mensatekAdapter->verify($pdfToVerify,$this->getMensatePdfConfiguration());
        $this->assertTrue($stampFile->validStamp);

    }

    public function testFailVerify(): void
    {

        $credential = new MensatekCredential($this->mtkUrl, $this->mtkUser, $this->mtkUser);
        $mensatekAdapter = new MensatekTsaPdfAdapter($credential);
        $pdfToVerify = StampFile::fromPath($this->pathExamples. '/example.pdf');
        $stampFile = $mensatekAdapter->verify($pdfToVerify,$this->getMensatePdfConfiguration());
        $this->assertFalse($stampFile->validStamp);

    }

    private function getMensatePdfConfiguration(): MensatekPdfConfig
    {
        $libPath = __DIR__ . '/../src/Lib/lofirmo_com.jar';
        return new MensatekPdfConfig(StampFile::fromPath($libPath), 'test_signed');

    }

    private function getValidAdapterConfig(string $folder): AdapterConfig
    {
        $config = new AdapterConfig(StampFolder::fromPath(__DIR__ . '/../var/' . $folder, true));

        $libPath = __DIR__ . '/../src/Lib/lofirmo_com.jar';
        $mensatekConfig = new MensatekPdfConfig(StampFile::fromPath($libPath), 'test_signed');


        $config->addExtraConfig(MensatekPdfConfig::CONFIG_NAME, $mensatekConfig);
        return $config;
    }

}

<?php
declare(strict_types=1);


use Insidesuki\Stamp\Adapters\AdapterConfig;
use Insidesuki\Stamp\StampFile;
use Insidesuki\Stamp\StampFolder;
use Insidesuki\Stamp\Tsa\Mensatek\MensatekCredential;
use Insidesuki\Stamp\Tsa\Mensatek\MensatekTsaAdapter;
use PHPUnit\Framework\TestCase;

class MensatekTsaAdapterTest extends TestCase
{

    private string $pathExamples;
    private mixed $mtkUrl;
    private mixed $mtkUser;
    private mixed $mtkPassword;
    private mixed $mtkJarPath;

    public function setUp(): void
    {
        $this->pathExamples = __DIR__.'/../src/Docs/examples';
        $this->mtkUrl = $_ENV['MTK_URL'];
        $this->mtkUser = $_ENV['MTK_USER'];
        $this->mtkPassword = $_ENV['MTK_PASSWORD'];
        $this->mtkJarPath = $_ENV['MTK_JAR_PATH'];

    }

    /**
     * @throws \Insidesuki\Stamp\Exceptions\FileDoesNotExistsException
     */
    public function testOkFileWasStamped():void{

        $pathDestination = __DIR__.'/../var/folderNameNewer';

        $mensatekAdapter = new MensatekTsaAdapter(
            new MensatekCredential($this->mtkUrl,$this->mtkUser,$this->mtkPassword)
        );


        $tsrStamp = $mensatekAdapter->stamp(
            StampFile::fromPath($this->pathExamples.'/input.txt'),
            new AdapterConfig(StampFolder::fromPath($pathDestination,true))
        );

        $this->assertTrue($tsrStamp->validStamp);



    }


    public function testFailVerifyBadTsr():void{

        $tsr =  $this->pathExamples = __DIR__.'/../src/Docs/examples/input.txt';

        $mensatekAdapter = new MensatekTsaAdapter(
            new MensatekCredential($this->mtkUrl,$this->mtkUser,$this->mtkPassword)
        );

        $fileResult = $mensatekAdapter->verify(StampFile::fromPath($tsr));
        $this->assertFalse($fileResult->validStamp);

    }

    public function testOkVerifyValidTsr():void{

        $tsr =  $this->pathExamples = __DIR__.'/../src/Docs/examples/response.tsr';

        $mensatekAdapter = new MensatekTsaAdapter(
            new MensatekCredential($this->mtkUrl,$this->mtkUser,$this->mtkPassword)
        );

        $fileResult = $mensatekAdapter->verify(StampFile::fromPath($tsr));
        $this->assertTrue($fileResult->validStamp);


    }


}

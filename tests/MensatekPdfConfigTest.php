<?php
declare(strict_types=1);


use Insidesuki\Stamp\StampFile;
use Insidesuki\Stamp\Tsa\Mensatek\MensatekPdfConfig;
use PHPUnit\Framework\TestCase;

class MensatekPdfConfigTest extends TestCase
{

    public function testOkMensatekConfig():void{

        $libPath = __DIR__.'/../src/Lib/lofirmo_com.jar';
        $config = new MensatekPdfConfig(StampFile::fromPath($libPath));
        $this->assertSame(realpath(__DIR__.'/../src/Lib/'),$config->pathJar);
        $this->assertSame('lofirmo_com.jar',$config->jarFileName);

    }


}

<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Tsa\Mensatek\Exceptions;

use RuntimeException;

class MensatekConfigurationDoesNotExistsException extends RuntimeException
{

    private function __construct(string $message)
    {
        parent::__construct(sprintf('CustomConfig %s, does not exists!!!', $message));
    }


    public static function byPdfConfig():self{

        throw new self('mensatek');
    }


}
<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Tsa\Mensatek\Exceptions;

use Exception;

class UnsupportFileException extends Exception
{

    public function __construct(string $message)
    {
        parent::__construct(sprintf('The file:%s, must be a PDF!!!', $message));
    }
}
<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Tsa\Mensatek\Exceptions;

use RuntimeException;

class MensatekCommandFailException extends RuntimeException
{

    public function __construct(string $command,string $message)
    {

        $details = sprintf('Error on cmd:"%s",with response:%s',$command,$message);
        parent::__construct($details);
    }

}
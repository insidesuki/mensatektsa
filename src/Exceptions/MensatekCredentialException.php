<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Tsa\Mensatek\Exceptions;

use RuntimeException;

class MensatekCredentialException extends RuntimeException
{


}
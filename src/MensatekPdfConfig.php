<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Tsa\Mensatek;

use Insidesuki\Stamp\Contracts\CustomConfig;
use Insidesuki\Stamp\StampFile;

class MensatekPdfConfig implements CustomConfig
{


    public const CONFIG_NAME = 'mensatekPdfConfig';
    public readonly string $jarFileName;
    public readonly string $pathJar;

    public function __construct(
        public readonly StampFile $jarFile,
        public readonly string $stampedPdfFileName = 'stamped.pdf'

    )
    {

        $this->jarFileName = $this->jarFile->baseName;
        $this->pathJar = $this->jarFile->folder->fullpath;

    }


}
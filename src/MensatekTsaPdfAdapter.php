<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Tsa\Mensatek;

use Insidesuki\Stamp\Adapters\AdapterConfig;
use Insidesuki\Stamp\Contracts\CustomConfig;
use Insidesuki\Stamp\Contracts\TsaAdapter;
use Insidesuki\Stamp\Exceptions\FileDoesNotExistsException;
use Insidesuki\Stamp\StampFile;
use Insidesuki\Stamp\Tsa\Mensatek\Exceptions\MensatekCommandFailException;
use Insidesuki\Stamp\Tsa\Mensatek\Exceptions\MensatekCredentialException;
use Insidesuki\Stamp\Tsa\Mensatek\Exceptions\UnsupportFileException;


class MensatekTsaPdfAdapter extends AbstractMensatekTsaAdapter implements TsaAdapter
{


    private const LOFIRMO_PATH = __DIR__ . '/Lib';
    private const COMMAND_BASE = 'cd %s && java -jar %s ';
    private const COMMAND_SIGN = ' -fe %s -fs %s.pdf -u %s -p %s';
    private const COMMAND_CHECK = ' -fe %s -check true';

    public string $fullPathJarFile;

    /**
     * @throws FileDoesNotExistsException
     * @throws UnsupportFileException
     */
    public function stamp(StampFile $file, AdapterConfig $config): StampFile
    {

        $this->initConfiguration($config);

        if (false === $file->isPdf()) {
            throw new UnsupportFileException($file->fullPath);
        }

        $pdfToStamp = $config->outputPath->fullpath . '/' . $this->mensatekPdfConfig->stampedPdfFileName;

        $command = sprintf(self::COMMAND_SIGN,
            $file->fullPath,
            $pdfToStamp,
            $this->mensatekCredential->username(),
            $this->mensatekCredential->password()
        );

        $this->runCommandPdf($command);
        return $this->verify(StampFile::fromPath($pdfToStamp.'.pdf'));


    }

    private function runCommandPdf(string $command): string
    {


        $commandBase = sprintf(self::COMMAND_BASE,
            $this->mensatekPdfConfig->pathJar,
            $this->mensatekPdfConfig->jarFileName
        );

        $commandToExecute = $commandBase . $command;


        $output = $this->runCommand($commandToExecute);

        if (str_contains($output, 'ERROR: Ha ocurrido un error al realizar el sello, las credenciales son incorrectas o no dispone de créditos suficientes')) {
            throw new MensatekCredentialException($output);
        }
        if (str_contains($output, '"Error"')) {
            throw new MensatekCommandFailException($commandToExecute, $output);
        }

        return $output;

    }


    /**
     * @throws FileDoesNotExistsException
     */
    public function verify(StampFile $file, ?CustomConfig $customConfig = null): StampFile
    {

        if ($customConfig instanceof CustomConfig) {
            $this->mensatekPdfConfig = $customConfig;
        }

        $command = sprintf(self::COMMAND_CHECK,
            $file->fullPath
        );

        $output = $this->runCommandPdf($command);
        $result = false;

        if (str_contains($output, '"Número de firmas en el PDF"="1"')) {
            $result = true;
        }
        return StampFile::fromStamped($file->fullPath, $result, $output);

    }

}
<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Tsa\Mensatek;

use Exception;
use Insidesuki\Stamp\Adapters\AdapterConfig;
use Insidesuki\Stamp\Contracts\TsaCredential;
use Insidesuki\Stamp\Tsa\Mensatek\Exceptions\MensatekConfigurationDoesNotExistsException;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

abstract class AbstractMensatekTsaAdapter
{

    protected MensatekPdfConfig $mensatekPdfConfig;

    public function __construct(
        protected readonly TsaCredential $mensatekCredential
    )
    {
    }


    protected function initConfiguration(AdapterConfig $config):void{

        try{
            $this->mensatekPdfConfig = $config->config(MensatekPdfConfig::CONFIG_NAME);
        }
        catch (Exception){
            throw MensatekConfigurationDoesNotExistsException::byPdfConfig();
        }

    }

    protected function runCommand(string $command): string
    {

        $process = Process::fromShellCommandline($command);
        $process->run();

        if (false === $process->isSuccessful()) {
            throw  new ProcessFailedException($process);
        }

        return $process->getOutput();

    }

}
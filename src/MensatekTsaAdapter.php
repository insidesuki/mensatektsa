<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Tsa\Mensatek;

use Insidesuki\Stamp\Adapters\AdapterConfig;
use Insidesuki\Stamp\Contracts\CustomConfig;
use Insidesuki\Stamp\Contracts\TsaAdapter;
use Insidesuki\Stamp\Exceptions\FileDoesNotExistsException;
use Insidesuki\Stamp\StampFile;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 *
 */
class MensatekTsaAdapter extends AbstractMensatekTsaAdapter implements TsaAdapter
{
    private const COMMAND_REQUEST = 'openssl ts -query -data %s -no_nonce -%s -out %s';
    private const COMMAND_STAMP = 'curl -H "Content-Type:application/timestamp-query" --data-binary @%s %s -o %s -u %s:%s';
    private const COMMAND_VERIFY = 'openssl ts -reply -in %s -text';

    /**
     * @throws FileDoesNotExistsException
     */
    public function stamp(StampFile $file, AdapterConfig $config): StampFile
    {

        $outputFolder = $config->outputPath->fullpath;

        // copy file to outputFolder only if not the same path

        if ($file->folder->fullpath !== $outputFolder) {
            $fileToStamp = $file->copy($outputFolder);
        } else {
            $fileToStamp = $file;
        }


        // create hash for request.tsr
        $outputFileTsq = $outputFolder . '/' . $config->tsqFile;
        $queryCommand = sprintf(self::COMMAND_REQUEST,
            $fileToStamp->fullPath,
            $config->algo,
            $outputFileTsq
        );

        $outputTsqProcess = $this->runCommand($queryCommand);
        $tsqFile = StampFile::fromPath($outputFileTsq);

        $outputFileTsr = $outputFolder.'/'.$config->tsrFile;

            // response tsr
        $responseCommand = sprintf(self::COMMAND_STAMP,
            $tsqFile->fullPath,
            $this->mensatekCredential->baseUrl(),
            $outputFileTsr,
            $this->mensatekCredential->username(),
            $this->mensatekCredential->password()

        );

        $outputTsrProcess = $this->runCommand($responseCommand);
        $tsrFile = StampFile::fromPath($outputFileTsr);

        return $this->verify($tsrFile);


    }


    public function verify(StampFile $file, ?CustomConfig $customConfig = null): StampFile
    {

        $verifyCommand = sprintf(self::COMMAND_VERIFY, $file->fullPath);

        try {
            $output = $this->runCommand($verifyCommand);
            return StampFile::fromStamped($file->fullPath, true, $output);
        } catch (ProcessFailedException $failedException) {

            $bodyException = $failedException->getMessage();

            if (str_contains($bodyException, 'asn1 encoding routines:asn1_d2i_read_bio:not enough data')) {
                return StampFile::fromStamped($file->fullPath, false, $bodyException);
            }

            throw $failedException;

        }


    }


}
<?php
declare(strict_types=1);

namespace Insidesuki\Stamp\Tsa\Mensatek;

use Insidesuki\Stamp\Contracts\TsaCredential;

class MensatekCredential implements TsaCredential
{

    private string $baseUrl;
    private string $username;

    private string $password;

    public function __construct(string $baseUrl, string $username, string $password)
    {
        $this->baseUrl = $baseUrl;
        $this->username = $username;
        $this->password = $password;
    }

    public function baseUrl(): string
    {
        return $this->baseUrl;
    }

    public function username(): string
    {
        return $this->username;
    }

    public function password(): string
    {
        return $this->password;
    }


}